<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Study"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="studies" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de estudios </h1>
    <p>
        <a href="<%= request.getContextPath()%>/study/create">Nuevo estudio</a>
    </p>
    <table>
        <tr>
            <th>Id</th>
            <th>Codigo</th>
            <th>Abreviatura</th>
            <th>Nombre</th>
            <th>Nombre Corto</th>
            <th>Acci�n</th>
        </tr>
        <%        Iterator<model.Study> iterator = studies.iterator();
            while (iterator.hasNext()) {
                Study study = iterator.next();%>
        <tr>
            <td><%= study.getId()%></td>
            <td><%= study.getCode()%></td>
            <td><%= study.getAbreviation()%></td>
            <td><%= study.getName()%></td>
            <td><%= study.getShortName()%></td>
            <td> 
                <a href="<%= request.getContextPath() + "/study/delete/" + study.getId()%>"> Borrar</a>
                <a href="<%= request.getContextPath() + "/study/edit/" + study.getId()%>"> Editar</a>
            </td>
        </tr>
        <%
            }
        %>          </table>

</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
