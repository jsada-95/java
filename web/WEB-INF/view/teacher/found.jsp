

<%@page import="model.Teacher"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<jsp:useBean id="teacher" class="Teacher" scope="request"/>  

<div id="content">        
    <h1>Localizar profesor</h1>
    <ul>
        <li><%= teacher.getId() %></li>
        <li><%= teacher.getName() %></li>
        <li><%= teacher.getSurname() %></li>
        <li><%= teacher.getPhone() %></li>
    </ul>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>