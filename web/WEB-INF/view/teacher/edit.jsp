<%@page import="model.Teacher"%>
<%@include file="/WEB-INF/view/header.jsp" %>
<jsp:useBean id="teacher" class="model.Teacher" scope="request"/>  

<div id="content">
    <% if(teacher.getId() == 0){%>
    <h1> Registro no encontrado </h1>
    <% } else { %>
    <h1>Edici�n de profesores </h1>
    <form action="<%= request.getContextPath()%>/teacher/update" method="post">
        <input name="id" type="hidden" value="<%= teacher.getId()%>"><br>
        <label>Nombre</label><input name="name" type="text" value="<%= teacher.getName()%>"><br>
        <label>Apellido</label><input name="surname" type="text" value="<%= teacher.getSurname()%>"><br>
        <label>Tel�fono</label><input name="phone" type="text" value="<%= teacher.getPhone() %>"><br>
        
        
        <input value="Guardar" type="submit"><br>
    </form>    
    <% } %>
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
