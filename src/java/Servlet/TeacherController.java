/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import static java.lang.Long.parseLong;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import model.Teacher;
import persistence.TeacherDAO;

/**
 *
 * @author usuario
 */
public class TeacherController extends BaseController {
    
    private static final int ELEMENTS_PER_PAGE = 2;
    private static final Logger LOG = Logger.getLogger(StudyController.class.getName());
    private TeacherDAO teacherDAO;

    public void index(String page) {
        
        int pageNum = 1;
        //objeto persistencia
        teacherDAO = new TeacherDAO();
        ArrayList<Teacher> teachers = null;
        
        pageNum = Integer.parseInt(page);
         
        //leer datos de la persistencia
        synchronized (teacherDAO) {
            teachers = teacherDAO.getAll(pageNum, ELEMENTS_PER_PAGE);
        } //para que no haya 2 conexiones a la vez
        request.setAttribute("teachers", teachers);
        String name = "index";
        LOG.info("En ModuleController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/teacher/index.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/teacher/create.jsp");
    }

    public void insert() throws IOException {

        //objeto persistencia
        teacherDAO = new TeacherDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Teacher teacher = loadFromRequest();
        
        synchronized (teacherDAO) {
            try {
                teacherDAO.insert(teacher);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
            }
        }
        redirect(contextPath + "/teacher/index/1");
    }

    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        TeacherDAO  teacherDAO = new TeacherDAO();
        Teacher teacher = teacherDAO.get(id);
        request.setAttribute("teacher", teacher);
        dispatch("/WEB-INF/view/teacher/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        TeacherDAO  teacherDAO = new TeacherDAO();
        Teacher teacher = loadFromRequest();
        teacherDAO.update(teacher);
        response.sendRedirect(contextPath + "/teacher/index/1");
        return;
    }

    public void delete(String idString) throws IOException, SQLException 
    {
        LOG.info("BORRANDO " + idString);
        long id = toId(idString);
        
        TeacherDAO  teacherDao = new TeacherDAO();
        teacherDao.delete(id);
        redirect(contextPath + "/teacher/index/1");
        
    }
  
    
    private Teacher loadFromRequest()
    {
        Teacher teacher = new Teacher();
        LOG.info("Crear modelo");
        


        teacher.setId(toId(request.getParameter("id")));
        teacher.setName(request.getParameter("name"));
        teacher.setSurname(request.getParameter("surname"));
        teacher.setPhone((int) toId(request.getParameter("phone")));
        
        LOG.info("Datos cargados");
        return teacher;
    }
    
    
    public void guardar(String teacherName) throws SQLException {

        HttpSession session = request.getSession(true);
        session.setAttribute("name", teacherName);

        redirect(contextPath + "/teacher/index/1");

    }
    public void seek() throws SQLException {
        dispatch("/WEB-INF/view/teacher/seek.jsp");
    }
    
     public void found() throws SQLException {
        teacherDAO = new TeacherDAO();
        Teacher teacher = new Teacher();
        long id;
        
        id = parseLong(request.getParameter("numero"));
        synchronized (teacherDAO) {
            teacher = teacherDAO.get(id);
        } 
        request.setAttribute("teacher", teacher);
        dispatch("/WEB-INF/view/teacher/found.jsp");

    }
}
