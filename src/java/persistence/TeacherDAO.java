/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import model.Teacher;

/**
 *
 * @author usuario
 */
public class TeacherDAO extends BaseDAO{
    public TeacherDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }
    public ArrayList<Teacher> getAll(int page, int elementsPerPage) {
        PreparedStatement stmt = null;
        ArrayList<Teacher> teachers = null;
        int offset = (page - 1) * elementsPerPage;
        
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from teachers LIMIT " + elementsPerPage + " OFFSET " + offset);
            ResultSet rs = stmt.executeQuery();
            teachers = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Teacher teacher = new Teacher();
                teacher.setId(rs.getLong("id"));
                teacher.setName(rs.getString("name"));
                teacher.setSurname(rs.getString("surname"));
                teacher.setPhone(rs.getInt("phone"));

                teachers.add(teacher);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return teachers;
    }
    public Teacher get(long id) throws SQLException {
        LOG.info("get(id)");
        Teacher teacher = new Teacher();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM teachers"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            teacher.setId(rs.getLong("id"));
            teacher.setName(rs.getString("name"));
            teacher.setSurname(rs.getString("surname"));
            teacher.setPhone(rs.getInt("phone"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return teacher;
    }
   
    public void insert(Teacher teacher) throws SQLException {
        PreparedStatement stmt = null;
        LOG.info("Crear DAO");
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO teachers(name, surname, phone)"
                + " VALUES(?, ?, ?)"
        );
        //stmt.setLong(1, 4); // teacher.getId()
        
        //LOG.info(""+teacher.getId());
        stmt.setString(1, teacher.getName());
        stmt.setString(2, teacher.getSurname());
        stmt.setInt(3, teacher.getPhone());

        stmt.execute();
        this.disconnect();
    }
     public void update(Teacher teacher) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE teachers SET "
                        + "name = ?, "
                        + "surname = ?, "
                        + "phone = ? "
                        + " WHERE id = ? "
        );
        stmt.setString(1, teacher.getName());
        stmt.setString(2, teacher.getSurname());
        stmt.setInt(3, teacher.getPhone());
        stmt.setLong(4, teacher.getId());
        LOG.info("id : " + teacher.getId());
        stmt.execute();
        this.disconnect();
        return;
    }
     public void delete(long id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM teachers"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        stmt.execute();
        this.disconnect();
    }
    
}
